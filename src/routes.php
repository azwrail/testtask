<?php
return [
        '~^$~' => [
        'class' => \MVC\Controllers\FileController::class,
        'method' => 'createFormForLoader'
    ],
    '~^add~' => [
        'class' => \MVC\Controllers\FileController::class,
        'method' => 'entry'
    ],
    '~^find~' => [
        'class' => \MVC\Controllers\FileController::class,
        'method' => 'search'
    ],

    ];