<?php


namespace MVC\Controllers;


use MVC\Repositories\FileRepository;
use MVC\Validations\FileValidation;
use MVC\Views\View;

class FileController
{

    protected $repository;

    protected $view;

    public function __construct()
    {
        $this->view = new View( __DIR__ . '/../Views/templates/');

        $this->repository = new FileRepository();
    }

    public function createFormForLoader(): void
    {
        $this->view->renderHtml('load.php');
    }

    public function createFormForSearcher():void
    {
        $this->view->renderHtml('search.php');
    }


    public function entry()
    {
        if (!empty($_FILES['attachment']['name'])) {

            return $this->loadFromLocal();

        } elseif (!empty($_POST['url'])) {

            return $this->loadfromUrl($_POST['url']);

        }

        $this->createFormForLoader();

    }

    public function loadFromLocal()
    {

        try {

            $file = $_FILES['attachment'];

            FileValidation::validation($file);

            $this->repository->loader($file);

            $this->setCookie($file['name']);

            return header('Location: http://searcher.loc/find');

        } catch (\Exception $e) {
            $this->view->renderHtml('error.php', ['errors' => $e->getMessage()]);
        }

    }

    /**
     * @param string $url
     */
    public function loadFromUrl(string $url)
    {
        try {

            $fileName = $this->repository->loaderURL($url);

            $this->setCookie($fileName);

            return header('Location: http://searcher.loc/find');

        } catch (\Exception $e) {
            $this->view->renderHtml('error.php', ['error' => $e->getMessage()]);
        }
    }

    public function search()
    {
        if (!empty($request = $_POST['find'])) {

            try {

                $entries = $this->repository->searcher($request, $_COOKIE['fileName']);

                $this->view->setExtraVars('results', $entries);

                $this->view->renderHtml('result.php');

            } catch (\Exception $e) {

                $this->view->renderHtml('error.php', ['errors' => $e->getMessage()]);
            }

        } else $this->createFormForSearcher();
    }


    private function setCookie(string $fileName)
    {
        setcookie('fileName', $fileName, time()+3600);
    }

}