<?php


namespace MVC\Validations;




class FileValidation
{

    /**
     * @var array
     *
     * Here you can change allowed extensions for your application
     */
    private static $allowedExtensions = ['TXT', 'txt', 'doc'];

    /**
     * @param array $file
     * @throws \Exception
     */
    public static function validation(array $file): void
    {
        self::isExtension($file);
        self::isErrIniSize($file);
        self::isErrHtmlSize($file);
        self::isErrPartial($file);
        self::isErrNoFile($file);
        self::isErrNoTempDir($file);
    }

    /**
     * @param array $file
     * @throws \Exception
     */
    private static function isExtension(array $file): void
    {
        $extension = pathinfo($file['name'], PATHINFO_EXTENSION);

        if (! in_array($extension, self::$allowedExtensions)) {
            throw new \Exception('Файл не допустимого расширения');
        }
    }

    /**
     * @param array $file
     * @throws \Exception
     */
    private static function isErrIniSize(array $file): void
    {
        if ($file['error'] == 1) {
            throw new \Exception('Превышен максимально допустимый размер файла');
        }
    }

    /**
     * @param array $file
     * @throws \Exception
     */
    private static function isErrHtmlSize(array $file): void
    {
        if ($file['error'] == 1) {
            throw new \Exception('Превышен максимально допустимый размер файла');
        }
    }

    /**
     * @param array $file
     * @throws \Exception
     */
    private static function isErrPartial(array $file): void
    {
        if ($file['error'] == 3) {
            throw new \Exception('Файл не был загружен полностью');
        }
    }

    /**
     * @param array $file
     * @throws \Exception
     */
    private static function isErrNoFile(array $file): void
    {
        if ($file['error'] == 4) {
            throw new \Exception('Файл не был загружен');
        }
    }

    /**
     * @param array $file
     * @throws \Exception
     */
    private static function isErrNoTempDir(array $file): void
    {
        if ($file['error'] == 6) {
            throw new \Exception('Загрузка файла не удалась');
        }
    }
}