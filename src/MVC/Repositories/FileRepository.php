<?php
namespace MVC\Repositories;

class FileRepository
{

    /**
     * @var string
     *
     * Here you must state the path to your local folder for uploading files
     */
    private $path = '';

    /**
     * @param array $file
     * @return bool
     * @throws \Exception
     */
    public function loader(array $file): bool
    {
        if (! move_uploaded_file($file['tmp_name'], $this->path . basename($file['name']) )) {
            throw new \Exception('Загрузка файла не удалась!');
        }

        return true;
    }

    /**
     * @param string $url
     * @return string
     * @throws \Exception
     */
    public function loaderURL(string $url): string
    {
        $source = fopen($url, 'rt');

        if (! $source) {
            throw new \Exception('По уканному пути файл не найден');
        }

        $fileName = basename($url);

        $path = $this->path . $fileName;

        $dest = fopen($path, 'wt');

        if (! $dest) {
            throw new \Exception('Не удалось открыть каталог на сервере');
        }

        while (! feof($source)) {

            fwrite($dest, fread($source, 4096));

        }

        fclose($dest);

        fclose($source);

        return $fileName;
    }

    /**
     * @param string $request
     * @param string $fileName
     * @return array
     * @throws \Exception
     */
    public function searcher(string $request, string $fileName): array
    {
        $files = scandir($this->path);

        if (! $files) {
            throw new \Exception('Директория с файлами пуста');
        }

        if (! in_array($fileName, $files)) {
            throw new \Exception('Файл отсутствует в директории!');
        }

        $file = $this->path . '/' . $fileName;

        $fileArray = file($file);

        return $this->findString($fileArray, $request);

    }

    /**
     * @param array $fileArray
     * @param string $request
     * @return array
     */
    private function findString(array $fileArray, string $request): array
    {
        $entries = [];

        foreach ($fileArray as $key => $string) {

            if (strpos($string, $request) === false) {

                continue;

            }

            $entries[$key] = strpos($string, $request);

        }

        return $entries;
    }

}