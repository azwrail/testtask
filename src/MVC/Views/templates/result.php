<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Результат поиска</title>
</head>
<body>

<?php

if ($results === []) { ?>

    <h2> Искомое не встречается в файле :( </h2>

    <?php die();
}

$count = count($results); ?>

<h2> Искомое встречается в файле <?= $count ?> раза(раз): </h2>

<?php

$index = 1;

foreach ($results as $string => $entries) { ?>

    <strong> <?= $index; ?> совпадение. </strong>

    <?php $string++ ?>

    <p> Искомое встречается в строке под номером <?= $string ?> </p>

    <?php $entries++ ?>

    <p> В позиции, начиная с <?= $entries ?> символа с начала строки. </p>

    <br>

    <?php

    $index++;

}

?>

</body>
</html>