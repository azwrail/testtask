<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Загрузите файл для поиска</title>
</head>
<body>

<form action="/add" method="post" enctype="multipart/form-data">

    <strong> <label for="file">Загрузите текстовый файл</label> </strong>

    <p>
        <input type="hidden" name="MAX_FILE_SIZE" value="30000">
        <input type="file" name="attachment" id="file">
    </p>

    <strong> <label for="url">Или укажите ссылку на текстовый файл</label> </strong>

    <p>
        <input type="url" name="url">
    </p>

    <input class="btn btn-primary" type="submit" value="Загрузить">


</body>
</html>
