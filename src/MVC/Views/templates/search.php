<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Введите слово для поиска</title>
</head>
<body>

<form action="/find" method="post" enctype="multipart/form-data">

    <?php
    if ($_COOKIE['fileName']) { ?>

        <strong> Что будем искать в <em> <?= $_COOKIE['fileName'] ?> </em> ? </strong>

    <?php } ?>

    <?php if (!empty($errors)) { ?>
        <div style="color: red"><?= $errors; ?> </div>
    <?php } ?>

    <p><input type="text" name="find" id="find" placeholder="Введите слово для поиска"></p>

    <input class="btn btn-primary" type="submit" value="Найти">

</form>

</body>
</html>
