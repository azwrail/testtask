<?php
namespace MVC\Views;

class View
{
    private $templatesPath;

    protected $extraVars = [];

    /**
     * View constructor.
     * @param string $templatesPath
     */
    public function __construct(string $templatesPath)
    {
        $this->templatesPath = $templatesPath;
    }

    /**
     * @param $name
     * @param $value
     */
    public function setExtraVars($name, $value)
    {
        $this->extraVars[$name] = $value;
    }

    /**
     * @param string $templateName
     * @param array $vars
     */
    public function renderHtml(string $templateName, array $vars = [])
    {
        extract($vars);
        extract($this->extraVars);

        include $this->templatesPath . '/' . $templateName;
    }

}