<?php
//spl_autoload_register(function (string $className) {
//    require_once __DIR__ . '/../src/' . $className . '.php';
//});
require __DIR__ . '/../vendor/autoload.php';
$route=$_GET['route'] ?? '';
$routes= require __DIR__ .  '/routes.php';

$isRouteFound=false;
foreach ($routes as $pattern => $controllerAndAction) {
    preg_match($pattern,$route,$matches);
    if(!empty($matches)){
        $isRouteFound=true;
        break;
    }
}
if(!$isRouteFound) {
    echo 'Страница не найдена';
    return;
}
unset($matches[0]);

    $controllerName = $controllerAndAction['class'];
    $actionName = $controllerAndAction['method'];
    $controller = new $controllerName();
    $controller->$actionName(...$matches);

