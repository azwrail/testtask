**This PHP Library for searching position of a string in a text file. Allows to add file from local or remote storage.**

**1. Prerequisites**

- PHP 7.2 or later.

**2. Installation**

- The Library can be installed using Composer by running the following command:

`composer require azwrail/searcher`

**3. Initialization**

- The library contains a simple html-form to be used for beginning of work.

- Before use this library you must state the path to your local directory for uploading files.

- Class `FileRepository.php` contains private property `$path` where you must write the path.